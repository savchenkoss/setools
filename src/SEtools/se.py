#! /usr/bin/env python

import subprocess
import numpy as np


class SECaller(object):
    """
    A class to wrap around a sextractor call
    """
    def __init__(self, config_name):
        self.config_name = config_name
        # Let's find a binary
        possible_names = ["sextractor", "sex", "source-extractor"]
        for name in possible_names:
            child = subprocess.Popen(f"which {name}", stdout=subprocess.DEVNULL,
                                     stderr=subprocess.STDOUT, shell=True)
            child.communicate()[0]
            if child.returncode == 0:
                self.sex_name = name
                print(f"Sextractor binary name is: {name}")
                break
        else:
            print("Sextractor not found. Theese names were checked:")
            print(possible_names)
            raise OSError

    def call(self, fits_file, params=None):
        # Prepare a call string
        call_string = f"{self.sex_name} {fits_file} "
        if params is not None:
            for key, value in params.items():
                call_string += f" -{key} {value}"

        # Call Sextractor
        child = subprocess.Popen(call_string, shell=True)
        child.communicate()[0]
        return child.returncode


class SExCatalogue(object):
    def __init__(self, catFileName):
        self.objectList = []
        self.legend = []
        self.inds = [-1]
        self.index = 0  # For iterations
        # Parse SE catalogue with arbitrary number of objects and parameters
        for line in open(catFileName):
            sLine = line.strip()
            obj = {}
            if sLine.startswith("#"):
                # legend line
                self.legend.append(sLine.split()[2])
                self.inds.insert(-1, int(sLine.split()[1]) - 1)
                continue
            params = [float(p) for p in line.split()]
            for i in range(len(self.legend)):
                b = self.inds[i]
                e = self.inds[i+1]
                if e == b + 1:
                    obj[self.legend[i]] = params[b]
                else:
                    obj[self.legend[i]] = params[b:e]
            self.objectList.append(obj)
        self.numOfObjects = len(self.objectList)

    def find_nearest(self, x, y):
        """ Returns nearest object to given coordinates"""
        nearest = min(self.objectList, key=lambda obj: np.hypot(x-obj["X_IMAGE"], y-obj["Y_IMAGE"]))
        dist = np.hypot(x-nearest["X_IMAGE"], y-nearest["Y_IMAGE"])
        if (dist < 5.0) and (nearest["FLUX_AUTO"] > 0.0):
            return nearest
        else:
            return None

    def get_median_value(self, parameter):
        return np.median([obj[parameter] for obj in self.objectList])

    def get_all_values(self, parameter):
        return np.array([obj[parameter] for obj in self.objectList])

    def __iter__(self):
        return self

    def __next__(self):
        """ Iteration method """
        if self.index < self.numOfObjects:
            self.index += 1
            return self.objectList[self.index-1]
        else:
            self.index = 1
            raise StopIteration
